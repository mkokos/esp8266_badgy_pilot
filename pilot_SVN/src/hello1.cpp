  /* e-paper display lib */\
#define  ARDUINO  120
//#define SS 15
#include <GxEPD.h>
#include <GxGDEH029A1/GxGDEH029A1.cpp>
#include <GxIO/GxIO_SPI/GxIO_SPI.cpp>
#include <GxIO/GxIO.cpp>
/* include any other fonts you want to use https://github.com/adafruit/Adafruit-GFX-Library */
#include <Fonts/FreeMonoBold9pt7b.h>
#include <Fonts/FreeSansBoldOblique24pt7b.h>
/* "Hello my name is" background image */
#include "hello.h"
/* WiFi  libs*/
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include <WiFiManager.h>

#include"Screens.h"


/* change this to your name, you may have to adjust font size and cursor location in showHello() to perfectly center the text */
const char* name = "Badgy";
int offsetX=0;
  int offsetY=15;
  int sizeOfMenuPos=64;
  const int displayWidth=128;
  int positionX=0;
  int positionY=0;
/* Always include the update server, or else you won't be able to do OTA updates! */
/**/const int port = 8888;
/**/ESP8266WebServer httpServer(port);
/**/ESP8266HTTPUpdateServer httpUpdater;
/*                                                                                */

/* Configure pins for display */
GxIO_Class io(SPI, SS, 0, 2);
GxEPD_Class display(io); // default selection of D4, D2

/* A single byte is used to store the button states for debouncing */
byte buttonState = 0;
byte lastButtonState = 0;   //the previous reading from the input pin
unsigned long lastDebounceTime = 0;  //the last time the output pin was toggled
unsigned long debounceDelay = 50;    //the debounce time

void showHello();
void configModeCallback (WiFiManager *myWiFiManager);
void showIP();
void showText(char *text);
void  showMainMenu(Screens e,int position);
void static showStart(screenName e ,int position);
void setup()
{  
  display.init();
  
  pinMode(1,INPUT_PULLUP); //down
  pinMode(3,INPUT_PULLUP); //left
  pinMode(5,INPUT_PULLUP); //center
  pinMode(12,INPUT_PULLUP); //right
  pinMode(10,INPUT_PULLUP); //up
  
  /* Enter OTA mode if the center button is pressed */
  if(digitalRead(5)  == 0){
    /* WiFi Manager automatically connects using the saved credentials, if that fails it will go into AP mode */
    WiFiManager wifiManager;
    wifiManager.setAPCallback(configModeCallback);
    wifiManager.autoConnect("STARA_24","kokoszuzu");
    /* Once connected to WiFi, startup the OTA update server if the center button is held on boot */
    httpUpdater.setup(&httpServer);
    httpServer.begin();
    showIP();
    while(1){
      httpServer.handleClient();
    }
  }
  showStart(MAINMENU,1); //show "Hello my name is" immediately on boot
}

void loop()
{  
  byte reading =  (digitalRead(1)  == 0 ? 0 : (1<<0)) | //down
                  (digitalRead(3)  == 0 ? 0 : (1<<1)) | //left
                  (digitalRead(5)  == 0 ? 0 : (1<<2)) | //center
                  (digitalRead(12) == 0 ? 0 : (1<<3)) | //right
                  (digitalRead(10) == 0 ? 0 : (1<<4));  //up
                  
  if(reading != lastButtonState){
    lastDebounceTime = millis();
  }
  if((millis() - lastDebounceTime) > debounceDelay){
    if(reading != buttonState){
      buttonState = reading;
      for(int i=0; i<5; i++){
        if(bitRead(buttonState, i) == 0){
          switch(i){
            case 0:
              //do something when the user presses down
              showText("You pressed the down button!");
              break;
            case 1:
              //do something when the user presses left
              showText("You pressed the left button!");
              break;
            case 2:
              //do something when the user presses center
              showText("You pressed the center button!");
              break;
            case 3:
              //do something when the user presses right
              showText("You pressed the right button!");
              break;
            case 4:
              //do something when the user presses up
              showText("You pressed the up button!");
              break;                              
            default:
              break;
          }
        }
      }
    }
  }
  lastButtonState = reading;
}

void configModeCallback (WiFiManager *myWiFiManager){
  display.setRotation(3); //even = portrait, odd = landscape
  display.fillScreen(GxEPD_WHITE);
  const GFXfont* f = &FreeMonoBold9pt7b ;
  display.setTextColor(GxEPD_BLACK);
  display.setFont(f);
  display.setCursor(0,50);
  display.println("Connect to Badgy AP");
  display.println("to setup your WiFi!");
  display.update();  
}

void showText(char *text)
{
  display.setRotation(3); //even = portrait, odd = landscape
  display.fillScreen(GxEPD_WHITE);
  const GFXfont* f = &FreeMonoBold9pt7b ;
  display.setTextColor(GxEPD_BLACK);
  display.setFont(f);
  display.setCursor(10,70);
  display.println(text);
  display.update();
}

void showIP(){
  display.setRotation(3); //even = portrait, odd = landscape
  display.fillScreen(GxEPD_WHITE);
  const GFXfont* f = &FreeMonoBold9pt7b ;
  display.setTextColor(GxEPD_BLACK);
  display.setFont(f);
  display.setCursor(0,10);

  String url = WiFi.localIP().toString() + ":"+String(port)+"/update";
  byte charArraySize = url.length() + 1;
  char urlCharArray[charArraySize];
  url.toCharArray(urlCharArray, charArraySize);

  display.println("You are now connected!");
  display.println("");  
  display.println("Go to:");
  display.println(urlCharArray);
  display.println("to upload a new sketch.");
  display.update();  
}

void showHello()
{
  display.setRotation(3); //even = portrait, odd = landscape
  display.fillScreen(GxEPD_WHITE);
  display.drawBitmap(hello, 0, 0, 296, 128, GxEPD_WHITE);
  const GFXfont* f = &FreeSansBoldOblique24pt7b;
  display.setTextColor(GxEPD_BLACK);
  display.setFont(f);
  display.setCursor(70,100);
  display.println(name);
  display.update();
}
 void  showMainMenu(Screens e,int position)
{
  // opcja 
  for(int i=0;i<e.sizeOfMove;i++){
  if (position==i){
    display.drawRect(positionX+offsetX,positionY+offsetY,sizeOfMenuPos,sizeOfMenuPos,GxEPD_BLACK);
    display.drawRect(positionX+1+offsetX,positionY+1+offsetY,sizeOfMenuPos-2,sizeOfMenuPos-2,GxEPD_WHITE);
    }
  display.drawBitmap(&(*(e.menuItems+i)->icon), positionX+2+offsetX, positionY+2+offsetY, sizeOfMenuPos-4, sizeOfMenuPos-4, GxEPD_WHITE);
  // display.drawBitmap(options, positionX+2+offsetX, positionY+2+offsetY, sizeOfMenuPos-4, sizeOfMenuPos-4, GxEPD_WHITE);
  // opcja 2
  positionX+=64;
  if (positionX>=displayWidth){
  positionX-=displayWidth;
  positionY+=64;}
  
  }
//   positionX=64;
//   positionY=0;
  
//   if (position==2){
//   display.drawRect(positionX+offsetX,positionY+offsetY,sizeOfMenuPos,sizeOfMenuPos,GxEPD_BLACK);
//   display.drawRect(positionX+1+offsetX,positionY+1+offsetY,sizeOfMenuPos-2,sizeOfMenuPos-2,GxEPD_WHITE);
//   }display.drawBitmap(garageClosed, positionX+2+offsetX, positionY+2+offsetY, sizeOfMenuPos-4, sizeOfMenuPos-4, GxEPD_WHITE);
//   // opcja 3
//   positionX=0;
//   positionY=64;
  
//   if (position==3){
//   display.drawRect(positionX+offsetX,positionY+offsetY,sizeOfMenuPos,sizeOfMenuPos,GxEPD_BLACK);
//   display.drawRect(positionX+1+offsetX,positionY+1+offsetY,sizeOfMenuPos-2,sizeOfMenuPos-2,GxEPD_WHITE);
//   }display.drawBitmap(garageClosed, positionX+2+offsetX, positionY+2+offsetY, sizeOfMenuPos-4, sizeOfMenuPos-4, GxEPD_WHITE);
//   // opcja 4
//   positionX=64;
//   positionY=64;
  
//   if (position==4){
//   display.drawRect(positionX+offsetX,positionY+offsetY,sizeOfMenuPos,sizeOfMenuPos,GxEPD_BLACK);
//   display.drawRect(positionX+1+offsetX,positionY+1+offsetY,sizeOfMenuPos-2,sizeOfMenuPos-2,GxEPD_WHITE);
//   }display.drawBitmap(garageClosed, positionX+2+offsetX, positionY+2+offsetY, sizeOfMenuPos-4, sizeOfMenuPos-4, GxEPD_WHITE);
//   // opcja 5
//   positionX=0;
//   positionY=128;
  
//   if (position==5){
//   display.drawRect(positionX+offsetX,positionY+offsetY,sizeOfMenuPos,sizeOfMenuPos,GxEPD_BLACK);
//   display.drawRect(positionX+1+offsetX,positionY+1+offsetY,sizeOfMenuPos-2,sizeOfMenuPos-2,GxEPD_WHITE);
//   }display.drawBitmap(garageClosed, positionX+2+offsetX, positionY+2+offsetY, sizeOfMenuPos-4, sizeOfMenuPos-4, GxEPD_WHITE);
//   // opcja 6
//   positionX=64;
//   positionY=128;
  
//   if (position==6){
//   display.drawRect(positionX+offsetX,positionY+offsetY,sizeOfMenuPos,sizeOfMenuPos,GxEPD_BLACK);
//   display.drawRect(positionX+1+offsetX,positionY+1+offsetY,sizeOfMenuPos-2,sizeOfMenuPos-2,GxEPD_WHITE);
//   }display.drawBitmap(childRoom, positionX+2+offsetX, positionY+2+offsetY, sizeOfMenuPos-4, sizeOfMenuPos-4, GxEPD_WHITE);
//   // opcja 7
//   positionX=0;
//   positionY=128+64;
  
//   if (position==7){
//   display.drawRect(positionX+offsetX,positionY+offsetY,sizeOfMenuPos,sizeOfMenuPos,GxEPD_BLACK);
//   display.drawRect(positionX+1+offsetX,positionY+1+offsetY,sizeOfMenuPos-2,sizeOfMenuPos-2,GxEPD_WHITE);
//   }display.drawBitmap(livingRoom, positionX+2+offsetX, positionY+2+offsetY, sizeOfMenuPos-4, sizeOfMenuPos-4, GxEPD_WHITE);
//   // opcja 8
//   positionX=64;
//   positionY=128+64;
  
//   if (position==8){
//   display.drawRect(positionX+offsetX,positionY+offsetY,sizeOfMenuPos,sizeOfMenuPos,GxEPD_BLACK);
//   display.drawRect(positionX+1+offsetX,positionY+1+offsetY,sizeOfMenuPos-2,sizeOfMenuPos-2,GxEPD_WHITE);
//   }display.drawBitmap(options, positionX+2+offsetX, positionY+2+offsetY, sizeOfMenuPos-4, sizeOfMenuPos-4, GxEPD_WHITE);
  }
  void static showStart(screenName e ,int position)
{
  display.setRotation(2); //even = portrait, odd = landscape
  display.fillScreen(GxEPD_WHITE);
  const GFXfont* f = &FreeMonoBold9pt7b ;
  display.setTextColor(GxEPD_BLACK);
  display.setFont(f);
  display.setCursor(0,10);
  

  showMainMenu(allScreens[e],1);

  display.update();  

}