  /* e-paper display lib */\
#define  ARDUINO  120
//#define SS 15
#include <GxEPD.h>
#include <GxGDEH029A1/GxGDEH029A1.cpp>
#include <GxIO/GxIO_SPI/GxIO_SPI.cpp>
#include <GxIO/GxIO.cpp>
#include <Fonts/FreeMonoBold9pt7b.h>
#include <Fonts/FreeSansBoldOblique24pt7b.h>
/* Configure pins for display */


#include"menu.h"
#include"icons.h"
;
GxIO_Class io(SPI, SS, 0, 2);
GxEPD_Class display(io); // default selection of D4, D2
const char* name = "Badgy";
int offsetX=0;
  int offsetY=15;
  int sizeOfMenuPos=64;
  const int displayWidth=128;
  int positionX=0;
  int positionY=0;


void showText(char *text)
{
  display.setRotation(3); //even = portrait, odd = landscape
  display.fillScreen(GxEPD_WHITE);
  const GFXfont* f = &FreeMonoBold9pt7b ;
  display.setTextColor(GxEPD_BLACK);
  display.setFont(f);
  display.setCursor(10,70);
  display.println(text);
  display.update();
}

void showIP(char urlCharArray[]){
  display.setRotation(3);
   //even = portrait, odd = landscape
  display.fillScreen(GxEPD_WHITE);
  const GFXfont* f = &FreeMonoBold9pt7b ;
  display.setTextColor(GxEPD_BLACK);
  display.setFont(f);
  display.setCursor(0,10);



  display.println("You are now connected!");
  display.println("");  
  display.println("Go to:");
  display.println(urlCharArray);
  display.println("to upload a new sketch.");
  display.update();  
}

void showHello()
{
  display.setRotation(3); //even = portrait, odd = landscape
  display.fillScreen(GxEPD_WHITE);
  display.drawBitmap(error, 0, 0, 296, 128, GxEPD_WHITE);
  const GFXfont* f = &FreeSansBoldOblique24pt7b;
  display.setTextColor(GxEPD_BLACK);
  display.setFont(f);
  display.setCursor(70,100);
  display.println(name);
  display.update();
}
 void showMenu(Screens e,int position)
{
    display.setRotation(2); //even = portrait, odd = landscape
  display.fillScreen(GxEPD_WHITE);
  const GFXfont* f = &FreeMonoBold9pt7b ;
  display.setTextColor(GxEPD_BLACK);
  display.setFont(f);
  display.setCursor(0,10);
  // opcja 
  for(int i=0;i<e.sizeOfMove;i++){
  if (position==i){
    display.drawRect(positionX+offsetX,positionY+offsetY,sizeOfMenuPos,sizeOfMenuPos,GxEPD_BLACK);
    display.drawRect(positionX+1+offsetX,positionY+1+offsetY,sizeOfMenuPos-2,sizeOfMenuPos-2,GxEPD_WHITE);
    }
  display.drawBitmap(&(*(e.menuItems+i)->icon), positionX+2+offsetX, positionY+2+offsetY, sizeOfMenuPos-4, sizeOfMenuPos-4, GxEPD_WHITE);
  // display.drawBitmap(options, positionX+2+offsetX, positionY+2+offsetY, sizeOfMenuPos-4, sizeOfMenuPos-4, GxEPD_WHITE);
  // opcja 2
  positionX+=64;
  if (positionX>=displayWidth){
  positionX-=displayWidth;
  positionY+=64;}
  
  }
  display.update();
//   positionX=64;
//   positionY=0;
  
//   if (position==2){
//   display.drawRect(positionX+offsetX,positionY+offsetY,sizeOfMenuPos,sizeOfMenuPos,GxEPD_BLACK);
//   display.drawRect(positionX+1+offsetX,positionY+1+offsetY,sizeOfMenuPos-2,sizeOfMenuPos-2,GxEPD_WHITE);
//   }display.drawBitmap(garageClosed, positionX+2+offsetX, positionY+2+offsetY, sizeOfMenuPos-4, sizeOfMenuPos-4, GxEPD_WHITE);
//   // opcja 3
//   positionX=0;
//   positionY=64;
  
//   if (position==3){
//   display.drawRect(positionX+offsetX,positionY+offsetY,sizeOfMenuPos,sizeOfMenuPos,GxEPD_BLACK);
//   display.drawRect(positionX+1+offsetX,positionY+1+offsetY,sizeOfMenuPos-2,sizeOfMenuPos-2,GxEPD_WHITE);
//   }display.drawBitmap(garageClosed, positionX+2+offsetX, positionY+2+offsetY, sizeOfMenuPos-4, sizeOfMenuPos-4, GxEPD_WHITE);
//   // opcja 4
//   positionX=64;
//   positionY=64;
  
//   if (position==4){
//   display.drawRect(positionX+offsetX,positionY+offsetY,sizeOfMenuPos,sizeOfMenuPos,GxEPD_BLACK);
//   display.drawRect(positionX+1+offsetX,positionY+1+offsetY,sizeOfMenuPos-2,sizeOfMenuPos-2,GxEPD_WHITE);
//   }display.drawBitmap(garageClosed, positionX+2+offsetX, positionY+2+offsetY, sizeOfMenuPos-4, sizeOfMenuPos-4, GxEPD_WHITE);
//   // opcja 5
//   positionX=0;
//   positionY=128;
  
//   if (position==5){
//   display.drawRect(positionX+offsetX,positionY+offsetY,sizeOfMenuPos,sizeOfMenuPos,GxEPD_BLACK);
//   display.drawRect(positionX+1+offsetX,positionY+1+offsetY,sizeOfMenuPos-2,sizeOfMenuPos-2,GxEPD_WHITE);
//   }display.drawBitmap(garageClosed, positionX+2+offsetX, positionY+2+offsetY, sizeOfMenuPos-4, sizeOfMenuPos-4, GxEPD_WHITE);
//   // opcja 6
//   positionX=64;
//   positionY=128;
  
//   if (position==6){
//   display.drawRect(positionX+offsetX,positionY+offsetY,sizeOfMenuPos,sizeOfMenuPos,GxEPD_BLACK);
//   display.drawRect(positionX+1+offsetX,positionY+1+offsetY,sizeOfMenuPos-2,sizeOfMenuPos-2,GxEPD_WHITE);
//   }display.drawBitmap(childRoom, positionX+2+offsetX, positionY+2+offsetY, sizeOfMenuPos-4, sizeOfMenuPos-4, GxEPD_WHITE);
//   // opcja 7
//   positionX=0;
//   positionY=128+64;
  
//   if (position==7){
//   display.drawRect(positionX+offsetX,positionY+offsetY,sizeOfMenuPos,sizeOfMenuPos,GxEPD_BLACK);
//   display.drawRect(positionX+1+offsetX,positionY+1+offsetY,sizeOfMenuPos-2,sizeOfMenuPos-2,GxEPD_WHITE);
//   }display.drawBitmap(livingRoom, positionX+2+offsetX, positionY+2+offsetY, sizeOfMenuPos-4, sizeOfMenuPos-4, GxEPD_WHITE);
//   // opcja 8
//   positionX=64;
//   positionY=128+64;
  
//   if (position==8){
//   display.drawRect(positionX+offsetX,positionY+offsetY,sizeOfMenuPos,sizeOfMenuPos,GxEPD_BLACK);
//   display.drawRect(positionX+1+offsetX,positionY+1+offsetY,sizeOfMenuPos-2,sizeOfMenuPos-2,GxEPD_WHITE);
//   }display.drawBitmap(options, positionX+2+offsetX, positionY+2+offsetY, sizeOfMenuPos-4, sizeOfMenuPos-4, GxEPD_WHITE);
  }

void initdisplay(){
display.init();}

void showAP (){
  display.setRotation(3); //even = portrait, odd = landscape
  display.fillScreen(GxEPD_WHITE);
  const GFXfont* f = &FreeMonoBold9pt7b ;
  display.setTextColor(GxEPD_BLACK);
  display.setFont(f);
  display.setCursor(0,50);
  display.println("Connect to Badgy AP");
  display.println("to setup your WiFi!");
  display.update();  
}

