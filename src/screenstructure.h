#include"icons.h";

extern menuItem mainMenu[]={
    //pos       name                icon            on clickdo         onLeft          onRight      onUp         onDown
    {         "livingroom",       livingRoom,       C_LIVINGROOM,      C_MENULEFT,     C_MENURIGHT, C_MENUUP,    C_MENUDOWN},
    {       "livingroom",         garageOpen,       C_GARAGE,          C_MAINMENU,     C_MAINMENU,  C_MAINMENU,  C_MAINMENU},
    {         "livingroom",       garageClosed,     C_MAINMENU,        C_MENULEFT,     C_MENURIGHT, C_MENUUP,    C_MENUDOWN},
    {        "livingroom",        error,            C_MAINMENU,        C_MENULEFT,     C_MENURIGHT, C_MENUUP,    C_MENUDOWN},
    {         "livingroom",       error,            C_MAINMENU,        C_MENULEFT,     C_MENURIGHT, C_MENUUP,    C_MENUDOWN},
    {         "livingroom",       error,            C_MAINMENU,        C_MENULEFT,     C_MENURIGHT, C_MENUUP,    C_MENUDOWN},
    {        "livingroom",        error,            C_MAINMENU,        C_MENULEFT,     C_MENURIGHT, C_MENUUP,    C_MENUDOWN},
    {        "livingroom",        options,          C_UPDATE,          C_MAINMENU,     C_MAINMENU,  C_MAINMENU,  C_MAINMENU}
};

extern Screens allScreens[]= {
// name     sizeOfMove  wskaznik
{MAINMENU,  8,          mainMenu},
{CHILD,     4,           mainMenu},
{LIVINGROOM,3,           mainMenu}//,
// {GARAGE,    0,           mainMenu} 
};